import React, { Component } from 'react';
import './miniCalculator.less';
import Button from "./Button";

class MiniCalculator extends Component {

  constructor(props) {
    super(props);
    this.state = {
      result: 0
    };
    this.updateResult = this.updateResult.bind(this);
  }

  updateResult(num) {
    console.log(num);
    this.setState({
      result: num,
    })
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result">{this.state.result}</span>
        </div>
        <div className="operations">
          <Button updateResult={this.updateResult} name={'加1'} sim={'+'} num={'1'} currentResult={this.state.result}/>
          <Button updateResult={this.updateResult} name={'减1'} sim={'-'} num={'1'} currentResult={this.state.result}/>
          <Button updateResult={this.updateResult} name={'乘2'} sim={'*'} num={'2'} currentResult={this.state.result}/>
          <Button updateResult={this.updateResult} name={'除2'} sim={'/'} num={'2'} currentResult={this.state.result}/>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

