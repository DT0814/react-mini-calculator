import React from 'react'

export default class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  updateResult() {
    switch (this.props.sim) {
      case '+':
        this.props.updateResult(Number.parseInt(this.props.currentResult) + Number.parseInt(this.props.num));
        break;
      case '-':
        this.props.updateResult(this.props.currentResult - this.props.num);
        break;
      case '*':
        this.props.updateResult(this.props.currentResult * this.props.num);
        break;
      case '/':
        this.props.updateResult(this.props.currentResult / this.props.num);
        break;
    }
  }

  render() {
    return (
      <button onClick={this.updateResult.bind(this)}>{this.props.name}</button>
    )
  }
}
